// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/StaticMeshComponent.h"
#include "Bullet.h"
#include "BulletShooterTower.h"

// Sets default values
ABulletShooterTower::ABulletShooterTower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));  //create a bank uscenecomponent, to mess with the relative location
	Scene->SetupAttachment(RootComponent);       //to alter property in blueprint, cannot do it with a camera that is directly attached to the component, 
												//so we create a blank component, attach to the meshcomponent first.
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(Scene);
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Volumn"));
	TriggerVolume->SetupAttachment(Scene);

	SpawnInterval = 1.5;
	SpawnCountdown = 0;
	isActive = false;
}

// Called when the game starts or when spawned
void ABulletShooterTower::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
// Spawn bullet from bullet shooter tower
void ABulletShooterTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (isActive) {          //if find a target, start shooting bullet
		SpawnCountdown -= DeltaTime;
		if (SpawnCountdown <= 0)
		{
			//Spawn bullet and reset countdown
			FVector tempLocation = GetActorLocation();
			tempLocation += FVector(0, 0, 190.0f);
			ABullet* tempReference = GetWorld()->SpawnActor<ABullet>(BulletObject, tempLocation, FRotator::ZeroRotator);
			tempReference->setTarget(targetActor);
			SpawnCountdown = SpawnInterval;                             //set SpawnCoundown back to the SpawnInterval
		}
		FVector TargetDirection = targetActor->GetActorLocation() - GetActorLocation();    

		//modify the tower to always face the actor, using the code given in lab4
		TargetDirection.Z = 0;
		TargetDirection.Normalize();

		FVector Forward = FVector(1, 0, 0);
		float Dot = FVector::DotProduct(Forward, TargetDirection);
		float Det = Forward.X * TargetDirection.Y + Forward.Y * TargetDirection.X;
		float Rad = FMath::Atan2(Det, Dot);
		float Degrees = FMath::RadiansToDegrees(Rad);
		SetActorRotation(FRotator(0, Degrees, 0));
	}
}

void ABulletShooterTower::checkOverlap(AEnemy* OtherActor) {
	if (OtherActor) {   //cannot include Otheractor==this
		//UE_LOG(LogTemp, Warning, TEXT("Overlap has begun"));
		targetActor = OtherActor;
		isActive = true;
	}

}




