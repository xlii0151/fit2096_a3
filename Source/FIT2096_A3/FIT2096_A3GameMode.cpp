// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FIT2096_A3GameMode.h"
#include "FIT2096_A3Character.h"
#include "UObject/ConstructorHelpers.h"

AFIT2096_A3GameMode::AFIT2096_A3GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	PrimaryActorTick.bCanEverTick=true;
	baseHp = 4;
}

void AFIT2096_A3GameMode::Tick(float DeltaTime)
{

}

void AFIT2096_A3GameMode::minusbaseHp()
{
	baseHp -= 1;
}
void AFIT2096_A3GameMode::addScore(int theScore)
{
	score += theScore;
}

void AFIT2096_A3GameMode::addDamaged() {
	enemyDamaged += 1;
}



