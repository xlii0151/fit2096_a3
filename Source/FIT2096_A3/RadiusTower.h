// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Enemy.h"
#include "RadiusTower.generated.h"

//class for Radius Tower. Its radius is represented by the trigger volume inside BP_radiustower
UCLASS()
class FIT2096_A3_API ARadiusTower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARadiusTower();
	UPROPERTY(EditAnywhere,BlueprintReadWrite)      
		UStaticMeshComponent* VisibleComponent;
	UPROPERTY(EditAnywhere)      
		int maxDamage;
	UPROPERTY(EditAnywhere)
		int minDamage;
	UPROPERTY(EditAnywhere)
		UBoxComponent* TriggerVolume;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	float SpawnCountdown;
	UPROPERTY(EditAnywhere)             
		float SpawnInterval;			//set to 1 to represent damage per second
	UPROPERTY(BlueprintReadWrite)
		TArray<AEnemy*> targetActorArray;	//it stores the targets that is in overlap with the tower.
	UFUNCTION(BlueprintCallable)
		void addTarget(AEnemy* target);		//when detect an enemy begins overlap, add to array. called in BP_radiustower
	UFUNCTION(BlueprintCallable)
		void removeTarget(AEnemy* target);   //when detect an enemy ends overlap and is in the array, removes it from array. called in BP_radiustower
	UMaterial* OffMaterial;					//when start attacking, material is set on inside BP_radiustower
};
