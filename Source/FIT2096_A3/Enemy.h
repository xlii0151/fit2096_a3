// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Materials/Material.h"
#include "Enemy.generated.h"


//enemy spawned from the EnemySpawner class. its locations and hp and color are sent by that class.
UCLASS()
class FIT2096_A3_API AEnemy : public AActor
{
	GENERATED_BODY()
public:	
	// Sets default values for this actor's properties
	AEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* VisibleComponent;
	UPROPERTY(BlueprintReadWrite)
		int hitPoint;
	UPROPERTY(BlueprintReadOnly)    
		int choice;					//determines the color in BP_enemy
	UPROPERTY(BlueprintReadWrite)
		bool reachedBase;			//update in BP_enemy
	int32 TargetCheckpoint;		    //variable for the current Checkpoints index

	TArray<FVector> Checkpoints;	//an array of waypoints

	void setCheckpoints(TArray<FVector> checkpoints, int boundary, int theHitpoint, int Choice);

	int thisBoundary;			    //the last index of Checkpoints array
	int thisSpeed;
	
	void takeDamage(int damage);
	
};
