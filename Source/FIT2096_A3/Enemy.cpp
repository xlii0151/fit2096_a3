// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/StaticMeshComponent.h"
#include "UObject\ConstructorHelpers.h"
#include "Enemy.h"

// Sets default values
AEnemy::AEnemy()
{
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));  //create a bank uscenecomponent
	Scene->SetupAttachment(RootComponent);       //we create a blank component, attach to the meshcomponent first. to alter property in blueprint
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(Scene);
	TargetCheckpoint = 0;	//the variable for the index of the checkpoints array
	thisSpeed = 300;        //speed is set to 300 in default
	hitPoint = 20;			//hp is set to 20 in default
	reachedBase = false;	//this is set to true in BP_enemy when itself reaches the base
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//navigate through the waypoints
	FVector TargetDirection = Checkpoints[TargetCheckpoint] - GetActorLocation();     //TargetDirection is the vector from the actor to the waypoint
	if (FVector::Dist(this->GetActorLocation(), Checkpoints[TargetCheckpoint]) >= 10.0f)
	{
		TargetDirection.Normalize();
		SetActorLocation(GetActorLocation() + TargetDirection * thisSpeed * DeltaTime);
	}

	else                //when actor is very close to the waypoint
	{
		SetActorLocation(Checkpoints[TargetCheckpoint]);
		if (GetActorLocation() == Checkpoints[thisBoundary])
		{	
			reachedBase = true;
			Destroy();
		}
		TargetCheckpoint++;

	}
	//modify the actor to always face the target waypoint, using the code given in lab4
	TargetDirection.Z = 0;
	TargetDirection.Normalize();

	FVector Forward = FVector(1, 0, 0);
	float Dot = FVector::DotProduct(Forward, TargetDirection);
	float Det = Forward.X * TargetDirection.Y + Forward.Y * TargetDirection.X;
	float Rad = FMath::Atan2(Det, Dot);
	float Degrees = FMath::RadiansToDegrees(Rad);
	SetActorRotation(FRotator(0, Degrees, 0));
}

//called after itself is spawned. set the hp, speed, scale. Choice is used to determine the color in BP_enemy.
void AEnemy::setCheckpoints(TArray<FVector> checkpoints, int boundary, int theHitpoint, int Choice)
{
	Checkpoints = checkpoints;
	thisBoundary = boundary;
	hitPoint = theHitpoint;
	choice = Choice;
	
	FVector currentScale = GetActorScale3D();
	FVector newScale = currentScale * (hitPoint / 20);
	SetActorScale3D(newScale);
	thisSpeed = 10000 / hitPoint;
}

//called inside BP_enemy
void AEnemy::takeDamage(int damage) {
	
	hitPoint -= damage;
	
	if (hitPoint <= 0)
	{
		Destroy();
	}
}

