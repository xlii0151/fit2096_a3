// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawner.h"

// Sets default values
AEnemySpawner::AEnemySpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StartText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("StartNumber"));
	StartText->SetHorizontalAlignment(EHTA_Center);
	StartText->SetWorldSize(150.0f);
	RootComponent = StartText;

	SpawnInterval = 3;        //spawn an actor every 3 seconds
	SpawnCountdown = 0;       //when it becomes 0, spawn an enemy
	Enemy1Life = 30;
	Enemy2Life = 40;
	Enemy3Life = 20;
	Enemy4Life = 25;
}

// Called when the game starts or when spawned
void AEnemySpawner::BeginPlay()
{
	Super::BeginPlay();
	StartText->SetText(TEXT("GO!"));
	
}

// Called every frame
void AEnemySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SpawnCountdown -= DeltaTime;
	elapsedTime += DeltaTime;
	
	//generate an enemy. waypoints number is fixed. the scene is divided to 5 areas, 
	//the waypoint position in each area is randomly generated.
	if (elapsedTime <= 20){
		if (SpawnCountdown <= 0)
		{
			Checkpoints.Empty();
			FVector position = FVector(FMath::RandRange(-1400.0f, 300.0f), FMath::RandRange(-1700.0f, -1200.0f), 226.0f);
			Checkpoints.Add(position);
			FVector position1 = FVector(FMath::RandRange(-1400.0f, 300.0f), FMath::RandRange(-1200.0f, -500.0f), 226.0f);
			Checkpoints.Add(position1);
			FVector position2 = FVector(FMath::RandRange(-1400.0f, 300.0f), FMath::RandRange(-500.0f, 200.0f), 226.0f);
			Checkpoints.Add(position2);
			FVector position3 = FVector(FMath::RandRange(-1400.0f, 300.0f), FMath::RandRange(200.0f, 1000.0f), 226.0f);
			Checkpoints.Add(position3);
			FVector position4 = FVector(FMath::RandRange(-1400.0f, 300.0f), FMath::RandRange(1000.0f, 1800.0f), 226.0f);
			Checkpoints.Add(position4);
			FVector position5 = FVector(FMath::RandRange(-1400.0f, 300.0f), 3170, 226.0f);
			Checkpoints.Add(position5);

			int choice = rand() % 4;

			AEnemy* tempReference = GetWorld()->SpawnActor<AEnemy>(SpawnObject1, this->GetActorLocation(), FRotator::ZeroRotator);
			if (choice == 0) {
				tempReference->setCheckpoints(Checkpoints, Checkpoints.Num() - 1, Enemy1Life, 0);				//pass Checkpoints to the specific object
			}
			else if (choice == 1) {
				tempReference->setCheckpoints(Checkpoints, Checkpoints.Num() - 1, Enemy2Life, 1);
			}
			else if (choice == 2) {
				tempReference->setCheckpoints(Checkpoints, Checkpoints.Num() - 1, Enemy2Life, 2);
			}
			else if (choice == 3) {
				tempReference->setCheckpoints(Checkpoints, Checkpoints.Num() - 1, Enemy4Life, 3);
			}


			SpawnCountdown = SpawnInterval; //set SpawnCoundown back to the SpawnInterval 3
		}
	}
	
}

