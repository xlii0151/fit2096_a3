// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FIT2096_A3GameMode.generated.h"


//this game mode class is used to store global variables base hitpoint, 
//current score and current enemies damaged within game. it changes the value in My Hud blueprint.
UCLASS(minimalapi)
class AFIT2096_A3GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFIT2096_A3GameMode();

	int baseHp;        //set to 4 lives in default.
	int score;
	int enemyDamaged;

	//these three functions is called inside MyHud blueprint, to update the text displayed in the hud.
	UFUNCTION(BlueprintPure)
		int32 getBaseHp() {
		return int32(baseHp);
		}
	UFUNCTION(BlueprintPure)
		int32 getScore() {
		return int32(score);
	}
	UFUNCTION(BlueprintPure)
		int32 getEnemyDamaged() {
		return int32(enemyDamaged);
	}

	//these three functions are called inside BP_enemy, to update variables
	UFUNCTION(BlueprintCallable)
		void minusbaseHp();
	UFUNCTION(BlueprintCallable)
		void addScore(int theScore);
	UFUNCTION(BlueprintCallable)
		void addDamaged();
	virtual void Tick(float DelatTime) override;
};



