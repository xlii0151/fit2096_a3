// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/StaticMeshComponent.h"
#include "UObject\ConstructorHelpers.h"
#include "RadiusTower.h"

// Sets default values
ARadiusTower::ARadiusTower()
{
 	
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));  
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(Scene);
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Volumn"));
	TriggerVolume->SetupAttachment(Scene);
	
	OffMaterial = CreateDefaultSubobject<UMaterial>(TEXT("Off Material"));
	ConstructorHelpers::FObjectFinder<UMaterial> OffMaterialObject(TEXT("/Game/StarterContent/Materials/RadiusTwoer_Off.RadiusTwoer_Off"));

	if (OffMaterialObject.Succeeded())
	{
		OffMaterial = OffMaterialObject.Object;
	}

	SpawnInterval = 1.0;
	SpawnCountdown = 0;
	minDamage = 3;
	maxDamage = 5;
}

// Called when the game starts or when spawned
void ARadiusTower::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ARadiusTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SpawnCountdown -= DeltaTime;
	if (SpawnCountdown <= 0)
	{
		//Spawn Actor and reset countdown
		for (int i = 0; i < targetActorArray.Num(); i++) {
			targetActorArray[i]->takeDamage(FMath::RandRange(minDamage, maxDamage));
			//UE_LOG(LogTemp, Warning, TEXT("current hit point is,%d"), damage);
		}
		SpawnCountdown = SpawnInterval;    //set SpawnCoundown back to the SpawnInterval
	}
}


void ARadiusTower::addTarget(AEnemy* target)
{
	targetActorArray.Add(target);
}

void ARadiusTower::removeTarget(AEnemy* target)
{
	if (targetActorArray.Contains(target)) {
		targetActorArray.Remove(target);
	}
	if (targetActorArray.Num() == 0) {
		VisibleComponent->SetMaterial(0, OffMaterial);
	}
}


