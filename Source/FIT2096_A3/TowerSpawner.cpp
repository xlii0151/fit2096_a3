// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerSpawner.h"

// Sets default values
ATowerSpawner::ATowerSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATowerSpawner::BeginPlay()
{
	Super::BeginPlay();

	//spawns towers in random place inside the level
	for (int i = 0; i <= 7; i++) {
		FVector position = FVector(FMath::RandRange(-1400.0f, 500.0f), FMath::RandRange(-1400.0f, 2000.0f), 226.0f);
		BulletTowerBornPlace.Add(position);
	}
	for (int i = 0; i <= 7; i++) {
		FVector position = FVector(FMath::RandRange(-1400.0f, 500.0f), FMath::RandRange(-1400.0f, 2000.0f), 226.0f);
		RadiusTowerBornPlace.Add(position);
	}
	SpawnTower();
}

// Called every frame
void ATowerSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void ATowerSpawner::SpawnTower()
{
	for (int i = 0; i < BulletTowerBornPlace.Num(); i++) {
		ABulletShooterTower* tempReference = GetWorld()->SpawnActor<ABulletShooterTower>(BulletTower, BulletTowerBornPlace[i], FRotator::ZeroRotator);
	}
	for (int i = 0; i < RadiusTowerBornPlace.Num(); i++) {
		ARadiusTower* tempReference = GetWorld()->SpawnActor<ARadiusTower>(RadiusTower, RadiusTowerBornPlace[i], FRotator::ZeroRotator);
	}
}

