// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enemy1.generated.h"

//please ignore this class, it is not used.
UCLASS()
class FIT2096_A3_API AEnemy1 : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemy1();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* VisibleComponent;
	int32 TargetCheckpoint;		//variable for Checkpoints index

	TArray<FVector> Checkpoints;	//an array of waypoints

	void setCheckpoints(TArray<FVector> checkpoints, int boundary, int theHitpoint, int Choice);

	int thisBoundary;			//the last index of Checkpoints array
	int thisSpeed;
	UPROPERTY(BlueprintReadWrite)
		int hitPoint;
	UPROPERTY(BlueprintReadOnly)
		int choice;
	void takeDamage(int damage);
};
