// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enemy.h"
#include "Bullet.generated.h"

//a bullet actor. it is spawned inside the BulletShooterTower class.
UCLASS()
class FIT2096_A3_API ABullet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABullet();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void setTarget(AEnemy* targetActor);	//set the target of the bullet
	AEnemy* theTargetActor;					
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* VisibleComponent;
	UPROPERTY(EditAnywhere)				//can be changed inside BP_bullet class
		float BulletSpeed;

	float MinumumDistance;				//when it is equal or less than the min distance, it hit the target.

	UPROPERTY(EditAnywhere)				//can be changed inside BP_bullet class
		int minDamage;
	UPROPERTY(EditAnywhere)
		int maxDamage;
};
