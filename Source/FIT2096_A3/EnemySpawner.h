// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TextRenderComponent.h"
#include "Enemy.h"
#include "EnemySpawner.generated.h"

//spawns the enemy, generate one of the 4 enemies.
UCLASS()
class FIT2096_A3_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawner();
	UPROPERTY(EditAnywhere)
		float SpawnInterval;
	UPROPERTY(EditAnywhere, Category = "SpawnObject1")
		TSubclassOf<AActor> SpawnObject1;
	
	//Variables to handle spawning
	bool StartSpawning = false;
	float SpawnCountdown;

	//Text Countdown
	UTextRenderComponent* StartText;

	//checkpoints are randomly created
	TArray<FVector> Checkpoints;

	//the life of the 4 kind of enemies can be changed in ue4.
	UPROPERTY(EditAnywhere)
		int Enemy1Life;
	UPROPERTY(EditAnywhere)
		int Enemy2Life;
	UPROPERTY(EditAnywhere)
		int Enemy3Life;
	UPROPERTY(EditAnywhere)
		int Enemy4Life;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	float elapsedTime;   //if elapsedTime reaches 20, stop spawning
};
