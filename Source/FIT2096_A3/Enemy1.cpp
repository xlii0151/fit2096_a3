// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/StaticMeshComponent.h"
#include "UObject\ConstructorHelpers.h"
#include "Enemy1.h"

// Sets default values
AEnemy1::AEnemy1()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));  //create a bank uscenecomponent, to mess with the relative location
	Scene->SetupAttachment(RootComponent);       //to alter property in blueprint, cannot do it with a camera that is directly attached to the component, 
												//so we create a blank component, attach to the meshcomponent first.
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(Scene);
	TargetCheckpoint = 0;	//the variable for the index of the checkpoints array
	thisSpeed = 300;  //speed is slower than second spawner because its larger
	hitPoint = 20;
}

// Called when the game starts or when spawned
void AEnemy1::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemy1::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector TargetDirection = Checkpoints[TargetCheckpoint] - GetActorLocation();     //TargetDirection is the vector from the actor to the waypoint
	if (FVector::Dist(this->GetActorLocation(), Checkpoints[TargetCheckpoint]) >= 10.0f)
	{
		TargetDirection.Normalize();
		SetActorLocation(GetActorLocation() + TargetDirection * thisSpeed * DeltaTime);
	}

	else                //when actor is very close to the waypoint
	{
		SetActorLocation(Checkpoints[TargetCheckpoint]);
		if (GetActorLocation() == Checkpoints[thisBoundary])
		{
			Destroy();
		}
		TargetCheckpoint++;

	}
	//modify the actor to always face the target waypoint, using the code given in lab4
	TargetDirection.Z = 0;
	TargetDirection.Normalize();

	FVector Forward = FVector(1, 0, 0);
	float Dot = FVector::DotProduct(Forward, TargetDirection);
	float Det = Forward.X * TargetDirection.Y + Forward.Y * TargetDirection.X;
	float Rad = FMath::Atan2(Det, Dot);
	float Degrees = FMath::RadiansToDegrees(Rad);
	SetActorRotation(FRotator(0, Degrees, 0));
}
void AEnemy1::setCheckpoints(TArray<FVector> checkpoints, int boundary, int theHitpoint, int Choice)
{
	Checkpoints = checkpoints;
	thisBoundary = boundary;
	hitPoint = theHitpoint;
	choice = Choice;
	FVector currentScale = GetActorScale3D();
	FVector newScale = currentScale * (hitPoint / 20);
	SetActorScale3D(newScale);
	thisSpeed = 10000 / hitPoint;
}

void AEnemy1::takeDamage(int damage) {
	UE_LOG(LogTemp, Warning, TEXT("previous hit point is,%d"), hitPoint);
	hitPoint -= damage;
	UE_LOG(LogTemp, Warning, TEXT("the damage is,%d"), damage);
	//UE_LOG(LogTemp, Warning, TEXT("Current hit point: "));
	UE_LOG(LogTemp, Warning, TEXT("current hit point is,%d"), hitPoint);
	if (hitPoint <= 0)
	{
		Destroy();
	}
}


