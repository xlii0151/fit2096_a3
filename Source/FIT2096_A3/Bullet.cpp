// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "Components/StaticMeshComponent.h"
#include "UObject\ConstructorHelpers.h"
// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObject(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	if (CubeMeshObject.Succeeded())
	{
		VisibleComponent->SetStaticMesh(CubeMeshObject.Object);
	}
	//VisibleComponent->SetMassScale(NAME_None,30.0f);
	BulletSpeed = 300;
	MinumumDistance = 60;
	minDamage = 4;
	maxDamage = 7;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (theTargetActor) {
		FVector TargetDirection = theTargetActor->GetActorLocation() - GetActorLocation();       
		if (TargetDirection.Size() > MinumumDistance)                                        //when the size of vector has not reached the minimum distance(ie. bump)
		{
			TargetDirection.Normalize();
			SetActorLocation(GetActorLocation() + TargetDirection * BulletSpeed * DeltaTime);     //chase the enemy in a constant speed
		}
		else
		{
			if (FMath::FRandRange(0, 1) < 0.75) {									//successful hit happened.
				//UE_LOG(LogTemp, Warning, TEXT("Damaged!"));
				theTargetActor->takeDamage(FMath::RandRange(minDamage, maxDamage));
				Destroy();
			}
			
		}
	}
	

	
}

void ABullet::setTarget(AEnemy* targetActor)
{
	theTargetActor = targetActor;
}

