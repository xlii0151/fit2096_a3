// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BulletShooterTower.h"
#include "RadiusTower.h"
#include "TowerSpawner.generated.h"

//this actor spawns all towers at the start of the game
UCLASS()
class FIT2096_A3_API ATowerSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, Category = "BulletTower")
		TSubclassOf<AActor> BulletTower;
	UPROPERTY(EditAnywhere, Category = "RadiusTower")
		TSubclassOf<AActor> RadiusTower;
	TArray<FVector> BulletTowerBornPlace;	//stores the positions of bullet towers
	TArray<FVector> RadiusTowerBornPlace;   //stores the positions of radius towers
	void SpawnTower();					//this function spawns the towers
};
