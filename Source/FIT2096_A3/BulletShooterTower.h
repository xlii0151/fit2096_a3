// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "Enemy.h"
#include "BulletShooterTower.generated.h"

//a bullet shooter tower. Spawned inside TowerSpawner class. it can shoot bullet.
//the bullet will trace the enemy within its overlap range.
UCLASS()
class FIT2096_A3_API ABulletShooterTower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABulletShooterTower();
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		UStaticMeshComponent* VisibleComponent;
	UPROPERTY(EditAnywhere)
		UBoxComponent* TriggerVolume;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere)
		float SpawnInterval;	          //the spawn interval of bullet
	UPROPERTY(EditAnywhere, Category = "BulletObject")
		TSubclassOf<AActor> BulletObject;
	float SpawnCountdown;
	UPROPERTY(BlueprintReadWrite)
		bool isActive;					//set to active in BP_bulletshootertower, when an enemy begins overlap 
										//and has not endoverlap yet. otherwise set to false.

	UPROPERTY(BlueprintReadWrite)
		AEnemy* targetActor;			//set to OtherActor inside checkOverlap function.
	UPROPERTY(BlueprintReadWrite)
		bool castToSucceed;
	UFUNCTION(BlueprintCallable)		//called when OtherActor can be cast to an enemy, to set the target enemy
		void checkOverlap(AEnemy* OtherActor);
};
